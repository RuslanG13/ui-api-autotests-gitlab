
package gitlabTests;

import java.util.ArrayList;

public class MemberPojo {

    private String agency;
    private String id;
    private String image;
    private ArrayList<String> launches;
    private String name;
    private String status;
    private String wikipedia;

    public MemberPojo(String agency, String id, String image, ArrayList<String> launches, String name, String status, String wikipedia) {
        this.agency = agency;
        this.id = id;
        this.image = image;
        this.launches = launches;
        this.name = name;
        this.status = status;
        this.wikipedia = wikipedia;
    }

    public MemberPojo() {
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<String> getLaunches() {
        return launches;
    }

    public void setLaunches(ArrayList<String> launches) {
        this.launches = launches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }
}
